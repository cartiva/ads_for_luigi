﻿-- transactions in the pay period against bs sale accounts
select a.control
from fin.fact_gl a
inner join fin.dim_account b on a.account_key = b.account_key
inner join bspp.sale_accounts c on b.account = c.account
inner join dds.dim_date e on a.date_key = e.date_key
  and e.biweekly_pay_period_sequence = (
    select biweekly_pay_period_sequence
    from dds.dim_date
    where the_date = current_date -1)
where a.post_status = 'Y'
group by a.control;

-- these are the ros & writers for the above transactions
-- in the old script, this is what populates bspp.bs_ros
SELECT ro, writer_id,
  coalesce((
    select
      case opcodekey
        when 26302 then '312'
        when 26303 then '311'
        when 26304 then '303'
        when 26305 then '317'
        when 26306 then '712'
        when 26307 then '403'
      end as alt_writer_id
    from ads.ext_fact_repair_order
    where ro = d.ro
      and opcodekey between 26302 and 26307), 'none') as alt_writer_id
FROM (
  SELECT a.ro, c.writernumber AS writer_id
  FROM ads.ext_fact_repair_order a
  INNER JOIN (
    select a.control
    from fin.fact_gl a
    inner join fin.dim_account b on a.account_key = b.account_key
    inner join bspp.sale_accounts c on b.account = c.account
    inner join dds.dim_date e on a.date_key = e.date_key
      and e.biweekly_pay_period_sequence = (
        select biweekly_pay_period_sequence
        from dds.dim_date
        where the_date = current_date -1)
    where a.post_status = 'Y'
    group by a.control) b on a.ro = b.control
  INNER JOIN ads.ext_dim_service_writer c on a.servicewriterkey = c.servicewriterkey
--   WHERE a.ro NOT IN ( -- exclude ros with > 2 writers -- this should not be necessary
--     SELECT a.ro
--     FROM ads.ext_fact_repair_order a
--     INNER JOIN (
--       select a.control
--       from fin.fact_gl a
--       inner join fin.dim_account b on a.account_key = b.account_key
--       inner join bspp.sale_accounts c on b.account = c.account
--       inner join dds.dim_date e on a.date_key = e.date_key
--         and e.biweekly_pay_period_sequence = (
--           select biweekly_pay_period_sequence
--           from dds.dim_date
--           where the_date = current_date -1)
--       where a.post_status = 'Y'
--       group by a.control) b on a.ro = b.control
--     INNER JOIN ads.ext_dim_opcode c on a.opcodekey = c.opcodekey
--       AND c.opcodekey BETWEEN 26302 AND 26307
--     GROUP BY a.ro
--     HAVING COUNT(*) > 1)
  GROUP BY a.ro, c.writernumber) d

select * from ops.ads_extract
delete from ops.ads_extract where task in ('ext_dim_customer','ext_dim_opcode','ext_dim_service_writer')

select sum(the_count)
from (
  select count(*) as the_count
  from ops.ads_extract
  where the_date = current_date
    and task = 'ads_ext_dim_opcode'
    and complete = TRUE
  union all
  select count(*)
  from ops.ads_extract
  where the_date = current_date
    and task = 'ads_ext_fact_repair_order'
    and complete = TRUE) a









  