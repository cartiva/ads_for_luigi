# coding=utf-8
import db_cnx
import csv
import ops
"""
9/25/17:
  vehicleInventoryItemStatuses: change to full scrape (back thru 2011)
11/13/17
    add vehicleevaluations
01/17/18
    add dds.dimsalesperson
    add dps.people
01/18/18
    add dps.vehicleItemStatuses

ext_vehicle_items
ext_vehicle_inventory_items
ext_vehicle_pricings
ext_vehicle_pricing_details
ext_make_model_classifications
ext_vehicle_inventory_item_statuses
ext_vehicle_item_mileages
ext_vehicle_sales
ext_vehicle_evaluations
ext_vehicle_acquisitions
ext_dim_salesperson
ext_people
ext_vehicle_item_statuses

"""
pg_con = None
ads_con = None
pg_server = '173'

#  ext_vehicle_items
task = 'uc_inventory_ext_vehicle_items'
try:
    file_name = 'files/ext_vehicle_items.csv'
    table_name = 'ads.ext_vehicle_items'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT vehicleitemid,vin,bodystyle,trim,interiorcolor,exteriorcolor,engine,
                  transmission,make,model,yearmodel,vinresolved
                from vehicleitems
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_vehicle_inventory_items
task = 'uc_inventory_ext_vehicle_inventory_items'
try:
    file_name = 'files/ext_vehicle_inventory_items.csv'
    table_name = 'ads.ext_vehicle_inventory_items'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT vehicleinventoryitemid,tablekey,basistable,stocknumber,fromts,
              coalesce(thruts, cast('12/31/9999 01:00:00' AS sql_timestamp)),
              locationid,vehicleitemid,bookerid,currentpriority,owninglocationid
            from vehicleinventoryitems
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_vehicle_pricings
task = 'uc_inventory_ext_vehicle_pricings'
try:
    file_name = 'files/ext_vehicle_pricings.csv'
    table_name = 'ads.ext_vehicle_pricings'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT *
            from vehiclepricings
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_vehicle_pricing_details
task = 'uc_inventory_ext_vehicle_pricing_details'
try:
    file_name = 'files/ext_vehicle_pricing_details.csv'
    table_name = 'ads.ext_vehicle_pricing_details'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT *
            from vehiclepricingdetails
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_make_model_classifications
task = 'uc_inventory_ext_make_model_classifications'
try:
    file_name = 'files/ext_make_model_classifications.csv'
    table_name = 'ads.ext_make_model_classifications'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT make,model,vehiclesegment,vehicletype,luxury,commercial,
              sport,fromts, coalesce(thruts, cast('12/31/9999 01:00:00' as sql_timestamp)),
              mfgorigintyp
            from MakeModelClassifications
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_vehicle_inventory_item_statuses
task = 'uc_inventory_ext_vehicle_inventory_item_statuses'
try:
    file_name = 'files/ext_vehicle_inventory_item_statuses.csv'
    table_name = 'ads.ext_vehicle_inventory_item_statuses'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT vehicleinventoryitemid,status,category,fromts,
              coalesce(thruts, cast('12/31/9999 01:00:00' as sql_timestamp)),
              basistable,tablekey,userid
            from vehicleinventoryitemstatuses
            where year(cast(fromts as sql_date)) > 2010
            --where fromts >= (
            --  select min(fromts)
            --  from vehicleinventoryitemstatuses
            --  where thruts is null)
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_vehicle_item_mileages
task = 'uc_inventory_ext_vehicle_item_mileages'
try:
    file_name = 'files/ext_vehicle_item_mileages.csv'
    table_name = 'ads.ext_vehicle_item_mileages'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT *
            from vehicleitemmileages
            where basistable <> 'BodyShopJobs'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

#  ext_vehicle_sales
task = 'uc_inventory_ext_vehicle_sales'
try:
    file_name = 'files/ext_vehicle_sales.csv'
    table_name = 'ads.ext_vehicle_sales'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
            SELECT *
            from vehiclesales
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_vehicle_evaluations
task = 'uc_inventory_ext_vehicle_evaluations'
try:
    file_name = 'files/ext_vehicle_evaluations.csv'
    table_name = 'ads.ext_vehicle_evaluations'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from vehicleevaluations
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_vehicle_acquisitions
task = 'uc_inventory_ext_vehicle_acquisitions'
try:
    file_name = 'files/ext_vehicle_acquisitions.csv'
    table_name = 'ads.ext_vehicle_acquisitions'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from vehicleacquisitions
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)
except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_dim_salesperson
task = 'uc_inventory_ext_dim_salesperson'
try:
    file_name = 'files/ext_dim_salesperson.csv'
    table_name = 'ads.ext_dim_salesperson'
    with db_cnx.ads_dds() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from dimsalesperson
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)

except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_people
task = 'uc_inventory_ext_people'
try:
    file_name = 'files/ext_people.csv'
    table_name = 'ads.ext_people'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                SELECT *
                from people
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """ truncate """ + table_name
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)

except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

# ext_vehicle_item_statuses
task = 'uc_inventory_ext_vehicle_item_statuses'
try:
    file_name = 'files/ext_vehicle_item_statuses.csv'
    table_name = 'ads.ext_vehicle_item_statuses'
    with db_cnx.ads_dps() as ads_con:
        with ads_con.cursor() as ads_cur:
            sql = """
                select *
                from VehicleItemStatuses
                where cast(fromts as sql_date) >= '01/01/2017'
            """
            ads_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(ads_cur.fetchall())
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            # sql = """ truncate """ + table_name
            sql = """ delete from """  + table_name + """ where extract(year from fromts) > 2016 """
            pg_cur.execute(sql)
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy """ + table_name + """ from stdin with csv encoding 'latin-1 '""", io)
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , TRUE
            """
            pg_cur.execute(sql)

except Exception, error:
    ops.email_error(task, 'uc_inventory', error)
    with db_cnx.new_pg(pg_server) as pg_con:
        with pg_con.cursor() as pg_cur:
            sql = """
                insert into ops.ads_extract (the_date, task, complete)
                select current_date, """ + "'" + task + "'" + """ , FALSE
            """
            pg_cur.execute(sql)

finally:
    if ads_con:
        ads_con.close()
    if pg_con:
        pg_con.close()
